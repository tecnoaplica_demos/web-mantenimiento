(function()
{
    //Stamp Labels
    var stamps = {
        new_version: "$_new_version"
    };

    //Global Configuration
    angular.module("config", []).constant("GLOBAL_CONFIGURATION",
    {
        //Application data
        application:
        {
            version: "1.0.0-rc.1",
            environment: "qas",
            language: "es",
            name: "TA - Mantenimiento",
            home: "app/home/dashboard/index"
        },

        on_build_new_version: function(newVersion, oldVersion)
        {
            //When has new Version , set the mark in the localstoage 
            localStorage.setItem(stamps.new_version, 1);
        },

        localstorageStamps:
        {
            new_version: stamps.new_version
        }

    });

})();
