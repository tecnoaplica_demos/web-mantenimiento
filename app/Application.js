﻿/*

    gale-material:          ANGULAR-GALE & ANGULAR-GALE-MATERIALLIBRARY
    app:                    CUSTOM PROJECT LIBRARY
    material-icons:         CUSTOM PROJECT BUNDLES (ADD SVG ICON'S)
    angularMoment:          ANGULAR MOMENT
    angularFileUpload:      ANGULAR FILE UPLOAD
    mocks:                  MOCKS ONLY FOR TESTING
    ui.router:              NG ROUTE
    uiGmapgoogle-maps:      GOOGLE MAPS
    chart.js:               ANGULAR CHART'S (http://jtblin.github.io/angular-chart.js/)
                                CHART OPTIONS: http://carlcraig.github.io/tc-angular-chartjs/doughnut/
*/

angular.module('App', [
        'gale-material',
        'ui.router',
        'app',
        'custom',
        'material-icons',
        'angularMoment',
        'angularFileUpload',
        'uiGmapgoogle-maps',
        //'n3-pie-chart',
        'mocks',
        'chart.js'
    ])
    .run(function($location) {

        //REDIRECT TO MAIN HOME (ONLY WHEN NO HAVE PATH)
        var currentPath = $location.url();
        var boot = $location.path("boot").search({
            path: currentPath
        });

        $location.url(boot.url());

    })
    .config(function($ApiProvider, $uploadFileProvider, CONFIGURATION) {
        //API Base Endpoint
        $ApiProvider.setEndpoint(CONFIGURATION.API_Endpoint);
        $uploadFileProvider.setFileEndpoint(CONFIGURATION.API_Endpoint + "Files/");
    })
    .config(function($IdentityProvider, SynchronizerProvider, MocksProvider) {
        //Security Provider
        $IdentityProvider
            .enable() //Enable
            .setIssuerEndpoint("Security/Authorize")
            .setLogInRoute("security/identity/login")
            .setWhiteListResolver(function(toState, current) {

                //Only Enable Access to Exception && Public State's
                if (toState.name.startsWith("boot") ||
                    toState.name.startsWith("exception.") ||
                    toState.name.startsWith("public.")) {
                    return true;
                }

                //Restrict Other State's
                return false;

            });

        //Synchronizer Manager
        SynchronizerProvider
            .autoLoadSynchronizers() //Auto Load Synchronizer via Reflection
            .frequency(45000); //Frequency between sync process

        //Mocking Module (While the API is in Construction)
        MocksProvider
        //.enable()
            .setDelay(700); //Simulate a Short Delay ^^, (More 'Real' experience)

    })
    .config(function(
        MocksProvider,
        SynchronizerProvider,
        ApplicationCleanseProvider,
        CONFIGURATION) {
        //Enable Debug for GPS and RouteTracker
        if (CONFIGURATION.debugging) {
            //Debugger Information
            MocksProvider.debug();
            SynchronizerProvider.debug();
            ApplicationCleanseProvider.debug();
        }

    })
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('pink')
            .warnPalette('red');

    })
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                // ---------------------------------------------
                // ONE-PAGE COLUMNS TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/default.html",
                controller: "DefaultLayoutController"
            })
            .state('plain', {
                url: "/plain",
                abstract: true,
                // ---------------------------------------------
                // REPORT TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/plain.html",
                controller: "DefaultLayoutController"
            })
            .state('exception', {
                url: "/exception",
                abstract: true,
                // ---------------------------------------------
                // EXCEPTION TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/exception.html",
                controller: "ExceptionLayoutController"
            })
            .state('public', {
                url: "/public",
                abstract: true,
                // ---------------------------------------------
                // PUBLIC TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/public.html",
                controller: "PublicLayoutController"
            });
        $urlRouterProvider.otherwise(function($injector, $location) {
            if ($location.path() !== "/") {
                var $state = $injector.get("$state");
                var $log = $injector.get("$log");

                $log.error("404", $location);
                $state.go("exception.error/404");
            }
        });
    })
    .config(function($logProvider, CONFIGURATION) {
        $logProvider.debugEnabled(CONFIGURATION.debugging || false);
    });
