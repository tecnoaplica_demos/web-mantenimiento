angular.module('mocks.api')

.run(function(Mocks)
{
    //-------------------------------------------------------------
    // Extend with some helping functions (for more real experience)
    var randomize = function(from, to)
    {
        return (Math.floor(Math.random() * to) + from);
    };


    Mocks.utility = {

        //For URL Mocking
        url:
        {
            parameter: function(url, name)
            {
                var sPageURL = url;
                var sURLVariables = sPageURL.split(/[&||?]/);
                var res;

                for (var i = 0; i < sURLVariables.length; i += 1)
                {
                    var paramName = sURLVariables[i];
                    var sParameterName = (paramName || '').split('=');

                    if (sParameterName[0] === name)
                    {
                        res = sParameterName[1];
                    }
                }

                return res;
            }
        },

        random:  
        {
            pick: function(array)
            {
                return array[randomize(0, array.length)];

            },

            between: randomize
        },

        paginate: function(offset, limit, total, createItemFunction)
        {
            var results = [];
            for (var index = 1; index <= limit; index++)
            {

                if ((offset + index) > total)
                {
                    continue;
                }

                var item = createItemFunction((offset + index));
                results.push(item);

            }

            return {
                offset: offset,
                limit: limit,
                total: total,
                items: results
            }
        }

    }


    //-------------------------------------------------------------
});
