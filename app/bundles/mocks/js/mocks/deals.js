angular.module('mocks.api')

.run(function(Mocks, $log, $location)
{
   return;
    
    //-------------------------------------------------------------
    Mocks.whenGET("Deals/Documents/Payer", function(method, url, data)
    {

        var randomDocuments = [];
        var total = Mocks.utility.random.between(1, 100);
        for (var random = 1; random < total; random++)
        {
            randomDocuments.push(
            {
                identifier: Mocks.utility.random.between(1000, 1500),
                selected: (Mocks.utility.random.between(1, 3) == 1 ? true : false),
                prices:
                {
                    value: Mocks.utility.random.between(1000000, 6868956),
                    rescue: Mocks.utility.random.between(9000000, 5868956),
                    retention: Mocks.utility.random.between(10000, 68689),
                    discount: Mocks.utility.random.between(1000, 6868),
                    sell: Mocks.utility.random.between(1200000, 8868956),
                },
                tax:  
                {
                    uptake: 0.50,
                    irf: 0.020
                }
            });
        }

        var result = {
            name: "Rodomillo S.A.",
            token: "e95f0240-88b6-404c-bb28-004a844f9bba",
            documents: randomDocuments
        };

        return [
            200,
            result,
            {}
        ];

    });
    //-------------------------------------------------------------

    //-------------------------------------------------------------
    Mocks.whenGET("Deals/Documents", function(method, url, data)
    {
        //Simulate Pagination
        var total = 5;
        var offset = parseInt(Mocks.utility.url.parameter(url, '$offset') || 0);
        var limit = parseInt(Mocks.utility.url.parameter(url, '$limit') || 10000);


        var payers = [
        {
            name: "Rodomillo S.A.",
            token: "e95f0240-88b6-404c-bb28-004a844f9bba"
        },
        {
            name: "Walmart",
            token: "0b0b9a8f-3e2e-45dc-a7ba-7ee81a45d70c"
        },
        {
            name: "Esval S.A.",
            token: "6bf924ec-c355-4a0c-88ff-ff6d0c17a065"
        },
        {
            name: "Redsol",
            token: "59a61176-5842-439f-8627-c93d80155c87"
        },
        {
            name: "Widul Ltda.",
            token: "1dbd641c-9042-401f-aa31-9f544b6e5d15"
        }];

        //Paging Simulation
        var result = Mocks.utility.paginate(offset, limit, total, function(index)
        {

            //----------------------------------------
            return {
                payer: payers[index - 1],
                documents:
                {
                    quantity: Mocks.utility.random.between(1, 26),
                    amount: Mocks.utility.random.between(1000000, 6868956)
                },
                lastInverter:
                {
                    name: "Primus Capital",
                    token: "b1bf74a2-9201-4621-9a5d-b23fde794f1b"
                },
                deals:
                {
                    quantity: Mocks.utility.random.between(1, 3)
                }
            };
            //----------------------------------------

        });

        return [
            200,
            result,
            {}
        ];
    });
    //-------------------------------------------------------------

    //-------------------------------------------------------------
    Mocks.whenGET("Deals", function(method, url, data)
    {
        var result = {
            token: "ef84be89-7903-4dbf-96ad-1db07693aca4",
            operation:
            {
                date: (new Date()).toISOString(),
                type:
                {
                    name: "OTC",
                    token: "abde7c4e-ea7c-4d69-862c-dda42519dc9d"
                }
            },
            documents:
            {
                total: 35
            },
            payCondition:
            {
                token: "3e7298c0-fa34-4621-b8a3-ab643b2d9b35",
                name: "Pago Hoy",
                identifier: "PH"
            },

            valuation:
            {
                uptake:
                {
                    value: 0.050,
                    clp: 340000000
                },
                irf:
                {
                    value: 0.020,
                    clp: 240000000
                }
            },

            investor:
            {
                name: "Consorcio",
                token: "b6b4b154-adbe-4326-a51a-b7ce28d02d7a"
            }
        };

        return [
            200,
            result,
            {}
        ];
    });
    //-------------------------------------------------------------
});
