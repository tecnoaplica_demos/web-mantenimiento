angular.module('mocks.api')

.run(function(Mocks, $log, $location)
{

    return;
    
    //-------------------------------------------------------------
    Mocks.whenGET("Inbox", function(method, url, data)
    {
        var RandomeDocumentsTypes = [
        {
            "identifier": "OFER",
            "name": "Oferta",
            "url": "app/deals/edit/index",
            "states": [
                "Pendiente de Generación",
                "Pendiente de Aprobación",
                "Objetada"
            ],
            "comments": [
                "Consorcio",
                "LV",
                "Inversor SA"
            ]
        },
        {
            "identifier": "SOCO",
            "name": "Solicitud de Compra",
            "url": "app/deals/edit/index",
            "states": [
                "Pendiente de Compra"
            ],
            "comments": [
                "Juan Garcia",
                "Pagador 2",
                "Pagador 3"
            ]
        }]

        //Simulate Pagination
        var total = 13;
        var offset = parseInt(Mocks.utility.url.parameter(url, '$offset') || 0);
        var limit = parseInt(Mocks.utility.url.parameter(url, '$limit') || 10000);

        //Paging Simulation
        var result = Mocks.utility.paginate(offset, limit, total, function(index)
        {
            //----------------------------------------
            // RANDOMIZE
            var selection = Mocks.utility.random.pick(RandomeDocumentsTypes);
            var selection_state = Mocks.utility.random.pick(selection.states);
            var selection_comment = Mocks.utility.random.pick(selection.comments);

            var dueDate = new Date();
            var numberOfDaysToAdd = Mocks.utility.random.between(1, 3);
            dueDate.setDate(dueDate.getDate() + numberOfDaysToAdd);

            return {
                name: selection.identifier + "-" + _.padLeft(index, 4, [chars = '0']),
                token: "d9ebeccb-9ece-44ec-b15a-18fad25411b2",
                dueDate: dueDate.toISOString(),
                comment: selection_comment,
                task:
                {
                    url: selection.url
                },
                type:
                {
                    name: selection.name,
                    token: "59c93d48-6f09-4759-a1fe-0fa56df6579d"
                },
                state:
                {
                    name: selection_state,
                    token: "8c512088-3530-4196-be97-1737bd7209b6"
                }
            };
            //----------------------------------------

        });

        return [
            200,
            result,
            {}
        ];
    });
    //-------------------------------------------------------------
});
