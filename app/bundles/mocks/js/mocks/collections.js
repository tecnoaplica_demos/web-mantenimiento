angular.module('mocks.api')

.run(function(Mocks, $log, $location)
{

    //-------------------------------------------------------------
    Mocks.whenGET("Collections/PayCondition", function(method, url, data)
    {
        var offset = parseInt(Mocks.utility.url.parameter(url, '$offset') || 0);
        var limit = parseInt(Mocks.utility.url.parameter(url, '$limit') || 10000);

        var items = [
        {
            token: "3e7298c0-fa34-4621-b8a3-ab643b2d9b35",
            name: "Pago Hoy",
            identifier: "PH"
        },
        {
            token: "71ae9e1b-73ec-4720-916d-1994f298973e",
            name: "Pago Mañana",
            identifier: "PM"
        },
        {
            token: "486f1bcf-4b6d-4707-a34f-4bd6f2f6fbda",
            name: "Contado Normal",
            identifier: "CN"
        }];

        return [
            200,
            {
                offset: offset,
                limit: limit,
                total: items.length,
                items: items
            },
            {}
        ];
    });
    //-------------------------------------------------------------

});
