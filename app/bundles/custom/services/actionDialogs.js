angular.module('custom.services')

.service('actionDialogs', function($mdDialog, $Localization)
{

    var delete_dialog = function(content)
    {
        if (!content)
        {
            $log.error("undefined 'content' for delete dialog");
        }

        return $mdDialog.show(
            $mdDialog.confirm()
            .ariaLabel('')
            .targetEvent(event)
            .title($Localization.get('GLOBAL.CONFIRM_DIALOG_DELETE_TITLE'))
            .ok($Localization.get('GLOBAL.CONFIRM_DIALOG_DELETE_OK_LABEL'))
            .cancel($Localization.get('GLOBAL.CONFIRM_DIALOG_DELETE_CANCEL_LABEL'))
            .content(content)
        );

    };


    var cancel_dialog = function(content)
    {
        if (!content)
        {
            content = $Localization.get('GLOBAL.CONFIRM_DIALOG_CANCEL_CONTENT_LABEL')
        }

        return $mdDialog.show(
            $mdDialog.confirm()
            .ariaLabel('')
            .targetEvent(event)
            .title($Localization.get('GLOBAL.CONFIRM_DIALOG_CANCEL_TITLE'))
            .ok($Localization.get('GLOBAL.CONFIRM_DIALOG_CANCEL_OK_LABEL'))
            .cancel($Localization.get('GLOBAL.CONFIRM_DIALOG_CANCEL_CANCEL_LABEL'))
            .content(content)
        );

    };


    return {
        delete: delete_dialog,
        cancel: cancel_dialog
    };
});
