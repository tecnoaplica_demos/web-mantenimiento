//------------------------------------------------------
// Company: Valentys Ltda.
// Author: dmunozgaete@gmail.com
// 
// Description: Application Bundle for Business Use
// 
// Use as you need =).
//------------------------------------------------------
angular.manifiest('custom', [
    'custom.directives',
    'custom.filters',
    'custom.services',
    'custom.components'
]);