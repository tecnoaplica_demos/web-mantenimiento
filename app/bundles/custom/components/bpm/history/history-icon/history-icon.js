angular.module('app.directives')

.directive('historyIcon', function($log, $mdSidenav, $Api, $q)
{
    return {
        restrict: 'E',
        require: ['ngModel', '^history-information'],
        scope:
        {
            ngModel: '=' //NG-MODEL Token Binding
        },
        templateUrl: 'bundles/custom/components/bpm/history/history-icon/history-icon.html',
        controller: function($scope, $element)
        {
            var icon = "";

            switch ($scope.ngModel.identifier)
            {
                case "CREA":
                    icon = "content:archive";
                    break;
                case "PEND":
                    icon = "action:history";
                    break;
                case "INTE":
                    icon = "communication:phonelink_erase";
                    break;
                case "ANUL":
                    icon = "av:not_interested";
                    break;
                case "PROG":
                    icon = "notification:event_available";
                    break;
                case "IREA":
                    icon = "editor:insert_comment";
                    break;
            };

            //---------------------------------------------
            // Model
            $scope.icon = icon;
            //---------------------------------------------

        }
    }
});
