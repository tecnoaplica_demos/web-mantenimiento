angular.module('app.directives')

.directive('$HistoryInformation', function($log, $mdSidenav, $Api, $q)
{
    return {
        restrict: 'E',
        scope:
        {
            documentToken: '=' //NG-MODEL Token Binding
        },
        templateUrl: 'bundles/custom/components/bpm/history/history-information.html',
        controller: function($scope, $element)
        {
            var defers = [];

            //---------------------------------------------
            // Model
            
            //---------------------------------------------


            //------------------------------------
            // Get Details Data
            defers.push(
                $Api.read("Documents/{company}/History",
                {
                    company: $scope.documentToken
                })
            );
            $q.all(defers).then(function(resolves)
            {
                //------------------------------------
                // History
                var history = resolves[0];
                
                $scope.history = history;

            });

            //---------------------------------------------
            // Directives Action's
            $scope.close = function()
            {
                $mdSidenav('right').close();
            };
            //---------------------------------------------
        },

        link: function(scope, element, attrs, ctrls)
        {
            //---------------------------------------------
            //---------------------------------------------
        }
    }
});
