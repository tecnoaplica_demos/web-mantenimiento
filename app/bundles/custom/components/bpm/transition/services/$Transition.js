// SERVICE
angular.module('custom.components')
    .provider('$Transition', function()
    {
        var $ref = this;

        this.$get = function($log, $q, $mdToast, $mdDialog, $loadingDialog, $Api)
        {
            var self = {};

            self.executeAction = function(settings)
            {
                var defer = $q.defer();

                //EXECUTE TRANSITION
                var execute = function(comment)
                {
                    $loadingDialog.show(
                    {
                        title: "Ejecutando Transición..."
                    });

                    $Api.update("Documents/{document}/Transition/{action}",
                        {
                            document: settings.document,
                            action: settings.action,
                            data:
                            {
                                observation: comment
                            }
                        })
                        .success(function()
                        {
                            defer.resolve();
                        })
                        .error(function(error)
                        {

                            $mdToast.show(
                                $mdToast.simple()
                                .content(error.error_description || error.error)
                                .position('bottom left')
                                .hideDelay(3000)
                            );

                            defer.reject(error);
                        })
                        .finally(function()
                        {
                            $loadingDialog.hide();
                        });

                };


                //BEFORE EXECUTE, SHOW A COMMENT DIALOG ;)
                if (settings.needObservation)
                {
                    self.show().then(function(comment)
                    {
                        execute(comment);
                    });
                }
                else
                {
                    execute();
                }

                return defer.promise;
            }

            //ADD NEW FACTORY
            self.show = function(config)
            {
                var deferred = $q.defer();
                var settings = {
                    controller: 'ObservationDialogController',
                    templateUrl: 'bundles/custom/components/bpm/transition/observation-dialog.tpl.html',
                    clickOutsideToClose: false,
                    escapeToClose: true,
                    focusOnOpen: true
                };

                $mdDialog.show(settings)
                    .then(function(data)
                    {
                        deferred.resolve(data);
                    }, function()
                    {
                        deferred.reject();
                    });

                return deferred.promise;
            };

            return self;
        };
    });
