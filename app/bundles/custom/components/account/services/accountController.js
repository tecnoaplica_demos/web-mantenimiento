//MODAL CONTROLLER
angular.module('custom.components')
    .controller('AccountDialogController', function(
        $scope,
        $log,
        $q,
        $mdDialog,
        $timeout,
        $Api
    )
    {
        //---------------------------------------------------
        // Model
        $scope.model = {
        };

        //-------------------------------------------
        // Action's
        $scope.save = function(model)
        {
            $mdDialog.hide(model);
        };

        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

    });
