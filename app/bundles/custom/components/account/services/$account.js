// SERVICE
angular.module('custom.components')
    .provider('$accountDialog', function()
    {
        var $ref = this;

        this.$get = function($log, $q, $mdDialog)
        {
            var self = {};

            //ADD NEW FACTORY
            self.show = function(ev)
            {
                var deferred = $q.defer();
                $mdDialog.show(
                    {
                        controller: 'AccountDialogController',
                        templateUrl: 'bundles/custom/components/account/account-dialog.tpl.html',
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        escapeToClose: true,
                        focusOnOpen: true
                    })
                    .then(function(data)
                    {
                        deferred.resolve(data);
                    }, function()
                    {
                        deferred.reject();
                    });

                return deferred.promise;
            };

            return self;
        };
    });
