/*------------------------------------------------------
 Company:           Valentys Ltda.
 Author:            David Gaete <dmunozgaete@gmail.com> (https://github.com/dmunozgaete)
 
 Description:       Notification Box for showing notification message's
                        - Facebook 
                        - Google
 Github:            https://github.com/dmunozgaete/angular-gale

 Versión:           1.0.0-rc.1
 Build Date:        2016-01-22 3:20:29
------------------------------------------------------*/
(function() {
    var _defaultDelay = 15000; //15 second's

    angular.module('app.components')

    .directive('notificationBox', function() {
        return {
            restrict: 'E',
            scope: {
                onNotification: '=', //Callback when authorization is completed
                onError: '=', //Callback when authorization fails
                onChangeState: '=' //Callback to send feedback 
            },
            templateUrl: 'bundles/app/components/notification-box/notification-box.tpl.html',
            controller: function(
                $scope,
                $element,
                $q,
                $state,
                $Identity,
                $Api,
                $LocalStorage,
                $rootScope,
                $log,
                $timeout,
                $mdMenu
            ) {
                var label = "notification_stamp";
                $scope.items = [];
                $scope.notification_counter = 0;

                $scope.showNotifications = function(){
                    //$state.go("app/bpm/notifications");
                };

                $scope.openMenu = function(directive, event) {
                    directive(event);

                    //Update Notification's to "READ"
                    if ($Identity.isAuthenticated()) {
                        var date = moment().toDate().toISOString();

                        //Get New's Notification's
                        $Api.update("Notifications/MarkAsReaded", {
                            timestamp: date
                        }).success(function() {
                            //Reset Counter
                            $scope.notification_counter = 0;
                            $rootScope.$broadcast("notifications.update-counter", 0); 
                        })
                    }
                };

                var getNotifications = function() {
                    var defer = $q.defer();
                    defer.promise.then(function(data) {
                        if (data.items.length > 0) {

                            //SET key with the date
                            angular.forEach(data.items, function(item) {
                                var icon = "";

                                //Decode Context
                                switch (item.type.identifier) {
                                    case "INFO":
                                        icon = "action:info_outline";
                                        break;
                                    case "RECH":
                                        icon = "action:report_problem";
                                        break;
                                    case "APRO":
                                        icon = "action:thumb_up";
                                        break;

                                }

                                item.icon = icon;
                            });

                            //Set new Stamp =)!
                            //$LocalStorage.set(label, data.timestamp);

                            //Call to update the notification's counter's
                            $rootScope.$broadcast("notifications.update-counter", data.items.length);

                            //Set Item's into box
                            $scope.items = data.items;
                            $scope.notification_counter = data.items.length;

                        }else{
                           //Reset Item's into box
                            $scope.items = [];
                            $scope.notification_counter = 0;

                            //Call to update the notification's counter's
                            $rootScope.$broadcast("notifications.update-counter", 0); 
                        }
                    });


                    //------------------------------------------------
                    //set or get the last time stamp 
                    var stamp = $LocalStorage.get(label);
                    if (!stamp) {
                        //If not exists, the first date is now- (1 months)
                        stamp = moment().subtract(1, 'M').toDate().toISOString();
                    }


                    // ONLY WHEN IS AUTHENTICATED
                    if ($Identity.isAuthenticated()) {

                        //Get New's Notification's
                        $Api.read("Notifications", {
                            timestamp: stamp
                        }).success(function(data) {

                            defer.resolve(data);

                        }).error(function() {
                            defer.reject();
                        });

                    } else {
                        defer.reject();
                    }
                    //------------------------------------------------

                    return defer.promise;
                };
                var update = function(delay) {
                    var timer = $timeout(function() {

                        $log.log("notification update");

                        $timeout.cancel(timer);

                        getNotifications().then(function(data) {
                            update(_defaultDelay);
                        }, function(error) {
                            update(_defaultDelay);
                        });

                    }, delay);
                };

                update(0); //First 0 second's

            }

        };
    });

})();
