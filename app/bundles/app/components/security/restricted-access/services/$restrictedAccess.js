/*------------------------------------------------------
 Company:           Valentys Ltda.
 Author:            David Gaete <dmunozgaete@gmail.com> (https://github.com/dmunozgaete)
 
 Description:       Service To check When the user is Authenticated Or Not
                    (Useful when user is Public, until some Authenticated Action
                    Must be executed)
 Github:            https://github.com/dmunozgaete/angular-gale

 Versión:           1.0.0-rc.1
 Build Date:        2016-01-22 3:20:29
------------------------------------------------------*/

angular.module('app.components')
    .provider('$restrictedAccess', function()
    {
        var $ref = this;

        this.$get = function($log, $q, $timeout, $Identity, $compile, $rootScope)
        {
            var self = {};

            //ADD NEW FACTORY
            self.validate = function(config)
            {
                var deferred = $q.defer();

                var delay = $timeout(function()
                {
                    //Clean
                    $timeout.cancel(delay);

                    if ($Identity.isAuthenticated())
                    {
                        deferred.resolve();
                        return;
                    }
                    else
                    {

                        //Need to create a directive element to avoid using 
                        //$mdDialog, because can't work with multiple dialog's
                        scope = $rootScope.$new();

                        scope.onAuthenticationFails = function(message)
                        {
                            deferred.reject(message);
                        };

                        scope.onAuthenticationSuccess = function(identity)
                        {
                            deferred.resolve(identity);
                        };

                        var dialog = $compile("<restricted-access-dialog class='ng-hide-animate ng-show-add' on-authentication-fails='onAuthenticationFails' on-authentication-success='onAuthenticationSuccess' />")(scope);
                        angular.element(document.body).append(dialog);

                    }

                }, 150);

                return deferred.promise;
            };

            return self;
        };
    });
