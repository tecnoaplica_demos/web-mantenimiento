/*------------------------------------------------------
 Company:           Valentys Ltda.
 Author:            David Gaete <dmunozgaete@gmail.com> (https://github.com/dmunozgaete)
 
 Description:       Graphical User Identification 
 Github:            https://github.com/dmunozgaete/angular-gale

 Versión:           1.0.0-rc.1
 Build Date:        2016-01-22 3:20:29
------------------------------------------------------*/

angular.module('app.components')

.directive('identityBox', function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'bundles/app/components/security/identity-box/identity-box.tpl.html',
        controller: function($scope, $Api, $element, $q, $Identity, $rootScope, $restrictedAccess) {
            var isAuthenticated = $scope.isAuthenticated = $Identity.isAuthenticated();
            if (isAuthenticated) {
                $scope.user = $Identity.getCurrent();

                $scope.userPhoto = "{0}/Accounts/{1}/Avatar".format([
                    $Api.getEndpoint(),
                    $scope.user.primarysid
                ]);
            }

            //--------------------------------------------
            // Action's
            $scope.logIn = function() {
                $restrictedAccess.validate();
            };

            $scope.logOut = function() {
                $Identity.logOut();
            };

            //--------------------------------------------
            // Event Hook's

            //When Authenticated State Change, Update Identity Box
            $rootScope.$on("auth-login-success", function(token) {
                isAuthenticated = $scope.isAuthenticated = true;
                $scope.user = $Identity.getCurrent();
            });

            $rootScope.$on("auth-logout-success", function() {
                isAuthenticated = $scope.isAuthenticated = false;
                delete $scope.user;
            });
        }
    };
});
