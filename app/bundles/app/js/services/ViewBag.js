angular.module('app.services')

.service('ViewBag', function()
{
    var self = {};
    var bag = {};


    self.add = function(data, identifier, prefix)
    {
        if (!identifier)
        {
            throw {
                message: 'identifier can\'t be empty'
            };
        }

        if (typeof prefix == "undefined")
        {
            prefix = identifier;
        };

        //Clone for disconnect , some $watch for ng-model
        var cloneData = angular.copy(data,
        {});

        //Add Step Data
        bag[identifier] = {
            prefix: prefix,
            fields: cloneData
        };
    };

    self.get = function(identifier)
    {
        return bag[identifier];
    };

    self.clear = function()
    {
        bag = {};
    };

    var iterator = function(resolver, callback)
    {
        //Each Section 
        for (var section in bag)
        {
            var configuration = bag[section];

            //Each Field
            for (var fieldName in configuration.fields)
            {
                var fullname = fieldName;
                if (configuration.prefix.length > 0)
                {
                    var fullname = "{0}_{1}".format([configuration.prefix, fieldName]);
                }

                var value = configuration.fields[fieldName];
                var resolverValue = undefined;

                if (resolver)
                {
                    resolverValue = resolver(value, fieldName, fullname);
                }

                if (typeof resolverValue != "undefined")
                {
                    value = resolverValue;
                }
                else
                {

                    if (typeof value == "object" && value.token)
                    {
                        value = value.token
                    }

                }

                if (typeof value == "undefined" || value == null)
                {
                    throw {
                        message: 'can\'t resolve value for ' + fullname
                    };
                }

                callback(fullname, value);
            }

        }
    };

    self.toObject = function(resolver)
    {
        var object = {};

        iterator(resolver, function(name, value)
        {
            object[name] = value;
        });
        
        return object;
    };

    self.toCollection = function(resolver)
    {
        var collection = [];

        iterator(resolver, function(name, value)
        {
            collection.push(
            {
                name: name,
                value: value
            })
        });

        return collection;
    };

    return self;
});
