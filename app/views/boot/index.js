angular.route('boot/index', function(
    $scope,
    $log,
    $Configuration,
    Synchronizer,
    $q,
    ApplicationCleanse,
    $Api,
    $LocalStorage,
    $location,
    $Identity
) {

    var stamps = $Configuration.get("localstorageStamps");
    var new_version_defer = $q.defer();
    var portal_information_defer = $q.defer();

    var onBooted = function() {
        Synchronizer.start().then(function() {
            // --------------------------------
            var path = $location.search().path;
            //Reset when path are in "boot" or "exception"
            if (path.length <= 2 ||
                path.indexOf("/boot") == 0 ||
                path.indexOf("exception") > 0) {
                var url = $Configuration.get("application");
                path = url.home;

                if ($Identity.isAuthenticated()) {
                    // If the user is "Portal User";
                    // redirect to his "inbox" special view 
                    var user = $Identity.getCurrent();
                    if (user.isInRole("PORT")) {
                        path = "/app/portal/resume";
                    }
                };
            }



            $location.url(path);
            // --------------------------------

        });

    };

    //When all Process are Checked, run APP
    $q.all([
        new_version_defer.promise,
        portal_information_defer.promise
    ]).then(onBooted, function(err) {
        $log.error(err);
    });


    // ---------------------------------------------------------
    // NEW VERSION SECTION! (ONLY WHEN NEW VERSION IS ACQUIRED)
    if ($LocalStorage.get(stamps.new_version)) {

        ApplicationCleanse.clean(true).then(function() {
            new_version_defer.resolve();
        }, function(err) {
            new_version_defer.reject(err);
        });

        //Remove new Version Flag
        $LocalStorage.remove(stamps.new_version);

    } else {
        new_version_defer.resolve();
    }
    // ---------------------------------------------------------

    // ---------------------------------------------------------
    // CHECK THE USER ROLE, IF A USER PORTAL , 
    // WHE NEED TO EXTRACT THE COMPANY ASSOCIATED
    if ($Identity.isAuthenticated()) {
        // If the user is "Portal User";
        // redirect to his "inbox" special view 
        var user = $Identity.getCurrent();
        if (user.isInRole("PORT")) {
            $Api.read("Portal/Me")
                .success(function(data) {
                    //EXTEND Identity to add the company for the Portal User
                    $Identity.extend("company", data);
                    portal_information_defer.resolve();
                })
                .error(function(data) {
                    console.error(arguments);
                });

        } else {
            portal_information_defer.resolve();
        }
    } else {
        portal_information_defer.resolve();
    }
    // ---------------------------------------------------------


});
