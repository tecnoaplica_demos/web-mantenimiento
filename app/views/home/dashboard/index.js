angular.route('app.home/dashboard/index', function(
    $scope,
    $log,
    $galeTable,
    $state,
    $Configuration,
    $stateParams,
    QueryableBuilder,
    $interval,
    $timeout,
    $Api,
    $q
) {

    $scope.model = {};

    var refresh = function() {
        //----------------------------------------------
        var defers = [
            $Api.read("Activities/Resume"),
            $Api.read("Activities/Pendings")
        ];
        $q.all(defers).then(function(resolves) {


            var resume = resolves[0];
            var pendings = resolves[1];

            var chart = {
                labels: [],
                data: [],
                colours: []
            };

            angular.forEach(resume.statistics, function(item) {
                chart.labels.push(item.label);
                chart.data.push(item.value);
                chart.colours.push(item.color);
            });

            //SET COLOR TO EACH ACTIVITY ACCORD OF HIS GENERAL STATE
            angular.forEach(pendings, function(pending) {
                var state = _.find(resume.statistics, {
                    identifier: pending.groupedState
                });
                if (state) {
                    pending.groupedColor = state.color;
                }
            });

            //Set Model
            $scope.model.chart = {
                labels: chart.labels,
                data: chart.data,
                colours: chart.colours,
                options: {
                    segmentStrokeColor: "#222",
                    percentageInnerCutout: 60,
                    segmentStrokeWidth: 3,
                    responsive: false,
                    maintainAspectRatio: false
                }
            };

            $scope.model.components = resume.components;
            $scope.model.pendings = pendings;


            $scope.model.waitingForApprovals = _.filter(pendings, function(pending) {
                return pending.state.identifier == "PAPR";
            }).length;


            $log.debug(resume.components);
            $log.debug(pendings);
        });
        //----------------------------------------------
    };
    var timer = $interval(refresh, 5000); // 15 second's
    var delay = $timeout(refresh, 500); // Delay for fix n3-Charts resize (http://stackoverflow.com/questions/26881811/n3-chart-width-not-set-according-to-parent-width)

    //----------------------------------------------------------
    $scope.details = function(item) {
        switch (item.state.identifier) {
            case "PAPR":
                $state.go('app.worker-orders/validate/index', {
                    workOrder: item.token
                });
                break;
            default:
                $state.go('app.worker-orders/view/index', {
                    workOrder: item.token
                });
                break;
        }
    };
    //----------------------------------------------------------

    //----------------------------------------------------------
    //DESTROY STEP
    $scope.$on("$destroy", function() {
        $interval.cancel(timer);
        $timeout.cancel(delay);
    });
    //----------------------------------------------------------

});
