angular.route('app.worker-orders/validate/index/:workOrder', function(
    $scope,
    $log,
    $state,
    $Configuration,
    $stateParams,
    $Api,
    $q,
    $Transition
) {
    //----------------------------------------------
    // MODEL's
    $scope.model = {};

    //----------------------------------------------
    // DEFER's
    var defers = [
        $Api.read("Activities/{workOrder}", {
            workOrder: $stateParams.workOrder
        }),
        $Api.read("Activities/{workOrder}/Report", {
            workOrder: $stateParams.workOrder
        })
    ];
    $q.all(defers).then(function(resolves) {
        var workOrder = resolves[0];
        var report = resolves[1];

        $scope.model.workOrder = workOrder;
        $scope.model.report = report;




        //Set Last GeoLocation
        $scope.model.map = {
            center: {
                latitude: report.latitude,
                longitude: report.longitude
            },
            zoom: 16,
            options: {
                panControl: false,
                streetViewControl: false,
                zoomControl: false,
                mapTypeControl: false
            }
        };

        $log.debug(workOrder);
        $log.debug(report);
    });

    //----------------------------------------------
    // ACTION's
    $scope.action = function(action, document)
    {
        var needObservation = false;
        switch (action)
        {
            case "APRO":
                break;
            case "RECH":
                needObservation = true;
                break;
        };

        //EXECUTE BPM TRANSACTION
        $Transition.executeAction(
        {
            document: document.token,
            action: action,
            needObservation: needObservation
        }).then(function()
        {
            $scope.back();
        });

    };

    $scope.back = function() {
        $state.go("app.home/dashboard/index");
    }
});
