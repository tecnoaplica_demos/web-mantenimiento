angular.route('app.worker-orders/view/index/:workOrder', function(
    $scope,
    $log,
    $state,
    $Configuration,
    $stateParams,
    $Api,
    $q
) {
    //----------------------------------------------
    // MODEL's
    $scope.model = {};

    //----------------------------------------------
    // DEFER's
    var defers = [
        $Api.read("Activities/{workOrder}", {
            workOrder: $stateParams.workOrder
        })
    ];
    $q.all(defers).then(function(resolves) {
        var workOrder = resolves[0];

        $scope.model.workOrder = workOrder;

        $log.debug(workOrder);
    });

    //----------------------------------------------
    // ACTION's
    $scope.back = function() {
        $state.go("app.home/dashboard/index");
    }
});
