angular.module('app.layouts').controller('DefaultLayoutController', function(
    $scope,
    $mdSidenav,
    $state,
    $log,
    $Configuration,
    $timeout,
    $Api,
    $galeFinder,
    $q,
    $location,
    $Identity,
    $rootScope,
    $mdToast
) {
    $scope.user = $Identity.getCurrent();
    $scope.userPhoto = "{0}/Accounts/{1}/Avatar".format([
        $Api.getEndpoint(),
        $scope.user.primarysid
    ]);
    //------------------------------------------------------------------------------------
    // Model
    $scope.config = {
        application: $Configuration.get("application")
    };

    //------------------------------------------------------------------------------------
    // Bootstrapping : Get the Menu 
    $Api.read("Accounts/Me/Menu")
        .success(function(data) {
            $scope.config.menu = data;
        })
        .error(function() {

        });

    //------------------------------------------------------------------------------------
    // Layout Actions
    $scope.showNotifications = function() {
        //$state.go("app/bpm/notifications");
    };
    $rootScope.$on("notifications.update-counter", function(event, counter) {
        $scope.notifications_counter = counter;
    });

    $scope.link = function(url) {
        $timeout(function() {
            $location.url(url);
        }, 300);
        $mdSidenav('layout-nav-left').close();
    };

    $scope.toogleMenu = function(side) {
        $mdSidenav(side).toggle();
    };

    $scope.throwError = function(error) {
        $mdToast.show(
            $mdToast.simple()
            .content(error.error_description || error.error)
            .position('bottom left')
            .hideDelay(3000)
        );

    };

    $scope.toggleSection = function(section) {
        section.open = !section.open;
    };

    $scope.navigateTo = function(item) {
        $scope.link(item.url);
    };

    $scope.logout = function() {
        $Identity.logOut();
    };
});
