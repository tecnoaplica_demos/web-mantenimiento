angular.route('app.accounts/index/:type', function(
    $scope,
    $log,
    $galeTable,
    $state,
    $Configuration,
    $stateParams,
    QueryableBuilder
)
{

    //------------------------------------------------------------
    //Table Setup
    $galeTable.then(function(component)
    {

        var kql = {
            filters: [
            {
                property: "type_identifier",
                operator: 'eq',
                value: 'USUA'
            }]
        };

        //Endpoint Setup
        component.setup(QueryableBuilder.build("Accounts", kql));

        //Row Click 
        component.$on("row-click", function(ev, item)
        {
            $state.go("app.accounts/edit",
            {
                token: item.token
            });
        });

    });

});
